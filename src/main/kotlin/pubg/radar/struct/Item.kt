package pubg.radar.struct

class Item {
  companion object {
    
    val category = mapOf(
        "Attach" to mapOf(
            "Weapon" to mapOf(
                "Lower" to mapOf(
                    "AngledForeGrip" to "A.Grip",
                        "Foregrip" to "V.Grip"
                ),
                "Magazine" to mapOf(
                    "Extended" to mapOf(
                        "Large" to "Ar.Ext",
                        "SniperRifle" to "S.Ext"),
                    "ExtendedQuickDraw" to mapOf(
                        "Large" to "Ar.ExtQ",
                        "SniperRifle" to "S.ExtQ"
                    )
                ),
                "Muzzle" to mapOf(
                    "Compensator" to mapOf(
                        "Large" to "Ar.Comp",
                        "SniperRifle" to "S.Comp"
                    ),
                    "FlashHider" to mapOf(
                        "Large" to "Ar.FlashHider",
                        "SniperRifle" to "S.FlashHider"
                    ),
                    "Suppressor" to mapOf(
                        "Large" to "Ar.Supp",
                        "SniperRifle" to "S.Supp"
                    )
                ),
                "Stock" to mapOf(
                    "AR" to "AR_Stock",
                    "SniperRifle" to mapOf(
                        "BulletLoops" to "S.Loops",
                        "CheekPad" to "CheekPad"
                    )
                ),
                "Upper" to mapOf(
                    "DotSight" to "R.Dot",
                    "Aimpoint" to "2x",
                    "ACOG" to "4x",
                    "CQBSS" to "8x"
                )
            )
        ),
        "Boost" to mapOf(
            "EnergyDrink" to "drink",
            "PainKiller" to "pain"
        ),
        "Heal" to mapOf(
            "FirstAid" to "heal",
            "MedKit" to "heal"
        ),
        "Weapon" to mapOf(
            "HK416" to "m4",
            "M16A4" to "m16",
            "Kar98K" to "98k",
            "Mini14" to "mini",
            "SCAR-L" to "scar",
            "AK47" to "ak",
            "SKS" to "sks",
            "Grenade" to "grenade"),
        "Ammo" to mapOf(
            "556mm" to "556",
            "762mm" to "762"),
        "Armor" to mapOf(
            "C" to mapOf("01" to mapOf("Lv3" to "armor3")),
            "D" to mapOf("01" to mapOf("Lv2" to "armor2"))),
        "Back" to mapOf(
            "C" to mapOf(
                "01" to mapOf("Lv3" to "bag3"),
                "02" to mapOf("Lv3" to "bag3")),
            "F" to mapOf(
                "01" to mapOf("Lv2" to "bag2"),
                "02" to mapOf("Lv2" to "bag2"))),
        "Head" to mapOf(
            "F" to mapOf(
                "01" to mapOf("Lv2" to "helmet2"),
                "02" to mapOf("Lv2" to "helmet2")),
            "G" to mapOf("01" to mapOf("Lv3" to "helmet3"))
        )
    ) as Map<String, Any>
    
    /**
     * @return null if not good, or short name for it
     */
    fun isGood(description: String): String? {
      try {
        val start = description.indexOf("Item_")
        if (start == -1) return null//not item
        val words = description.substring(start + 5).split("_")
        var c = category
        for (word in words) {
          if (word !in c)
            return null
          val sub = c[word]
          if (sub is String)
            return sub
          c = sub as Map<String, Any>
        }
      } catch (e: Exception) {
      }
      return null
    }
    
  }
}